// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass } = cc._decorator;

@ccclass
export default class Demo extends cc.Component {

    start() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart.bind(this), this)
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove.bind(this), this)
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnd.bind(this), this)
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel.bind(this), this)
    }

    update(deltaTime: number) {

    }

    private touchStart(event) {
        console.error("Touch")
    }
    private touchMove(event) {
        console.log("move")
    }
    private touchEnd(event) {
        console.warn("end")
    }
    private touchCancel(event) {
        console.warn("cancel")
    }
}
